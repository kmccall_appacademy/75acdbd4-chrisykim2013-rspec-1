def translate(str)
  res = []
  punc = [".", ",", "!", "?"]
  str.split.each do |word|
    if word == word.capitalize && punc.include?(word[-1])
      res << piglatinize(word[0..-2]).capitalize + "#{word[-1]}"
    elsif word == word.capitalize
      res << piglatinize(word).capitalize
    elsif punc.include?(word[-1])
      res << piglatinize(word[0..-2]) + "#{word[-1]}"
    elsif
      res << piglatinize(word)
    end
  end
  res.join(" ")
end


def piglatinize(word)
  vowels = ["a", "e", "i", "o", "u"]
  arr = word.downcase.chars

  if !anyvowels?(word)
    return arr.rotate(1).join + "ay"
  end

  arr.each.with_index do |ch, i|
    if ch == "q" && arr[i+1] == "u"
      return arr.rotate(i+2).join + "ay"
    elsif vowels.include?(ch)
      return arr.rotate(i).join + "ay"
    end
  end
end

def anyvowels?(word)
  vowels = ["a", "e", "i", "o", "u"]
  count = 0
  word.downcase.chars.each do |ch|
    if vowels.include?(ch)
      count += 1
    end
  end
  count > 0
end
    
