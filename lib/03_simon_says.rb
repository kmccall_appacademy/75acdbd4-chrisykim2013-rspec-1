def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, num = 2)
  res = []
  until res.length == num
    res << str
  end
  res.join(" ")
end

def start_of_word(str, num)
  str[0..num-1]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  arr = str.split
  res = [arr[0].capitalize]
  arr[1..-1].each do |word|
    if little?(word)
      res << word
    else
      res << word.capitalize
    end
  end
  res.join(" ")
end

def little?(str)
  ["the", "over", "and"].include?(str)
end
