def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  if arr.empty?
    0
  else
  arr.reduce(:+)
  end
end

def multiply(*numarr)
  numarr.reduce(:*)
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  if num == 0
    0
  else
  (1..num).to_a.reduce(:*)
  end
end
