require 'byebug'

def combine_word(word1, word2 = "")
  return word1 if word2.nil?
  word1 + word2
end

def combine_arr(arr)
  # debugger
  new_arr = []
  arr.each.with_index do |word, i|
    next if i.odd?
    new_arr << combine_word(arr[i],arr[i+1])
  end
  new_arr.join(" ")
end
