def ftoc(degree)
  (degree.to_f - 32) * 5 / 9
end

def ctof(degree)
  (9 * degree.to_f / 5) + 32
end
