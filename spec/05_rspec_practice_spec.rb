require '05_rspec_practice'

describe "combine_word" do
  it "returns two words combined with no spaces" do
    expect(combine_word("hello","world")).to eq("helloworld")
  end

  it "returns just the first word if second word is not provided" do
    expect(combine_word("hello")).to eq("hello")
  end
end

describe "combine_arr" do
  it "returns each pair in odd length arr combined into new sentence" do
    expect(combine_arr(["hello", "world", "!"])).to eq("helloworld !")
  end

  it "returns each pair in even length arr combined into new sentence" do
    expect(combine_arr(["hello", "world", "Chris", "Kim"])).to eq ("helloworld ChrisKim")
  end
end
